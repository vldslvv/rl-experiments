from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from collections import deque

import gym
import numpy as np
from scores import ScoreLogger
import random


GAMMA = 0.95
LEARNING_RATE = 0.001

MEMORY_SIZE = 1000000
BATCH_SIZE = 20

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995

ENV_NAME = "CartPole-v1"


class DQN:
    def __init__(self, observation_space, action_space):
        self.exploration_rate = EXPLORATION_MAX
        self.action_space = action_space
        self.memory = deque(maxlen=MEMORY_SIZE)
        
        self.model = Sequential()
        
        self.model.add(Dense(24, input_shape=(observation_space,), activation='relu'))
        self.model.add(Dense(24, activation='relu'))
        self.model.add(Dense(action_space, activation='linear'))
        self.model.compile(loss='mse', optimizer=Adam(lr=LEARNING_RATE))
        
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))
        
    def act(self, state):
        if np.random.rand() < self.exploration_rate:
            return random.randrange(self.action_space)
        
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])
    
    def replay_train(self):        
        if len(self.memory) < BATCH_SIZE:
            return
        batch = random.sample(self.memory, BATCH_SIZE)
        X_batch, y_batch = [], []

        for state, action, reward, next_state, done in batch:
            q_update = reward
            if not done:
                q_pred = np.amax(self.model.predict(next_state)[0])
                q_update = reward + (GAMMA * q_pred)
            q_values = self.model.predict(state)
            q_values[0][action] = q_update

            X_batch.append(state[0])
            y_batch.append(q_values[0])

        self.model.fit(np.array(X_batch), np.array(y_batch), verbose=0)
        self.exploration_rate *= EXPLORATION_DECAY
        self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)


def simulate(seed=None):
    env = gym.make(ENV_NAME)
    #env = gym.wrappers.Monitor(env, '../data/cartpole-1', force=True)
    env.seed(seed)
    score_logger = ScoreLogger(ENV_NAME)
    observation_space = env.observation_space.shape[0]
    action_space = env.action_space.n
    dqn = DQN(observation_space=observation_space, action_space=action_space)
    run = 0
    while True:
        run += 1
        state = env.reset()
        state = np.reshape(state, [1, observation_space])
        step = 0
        print(state)
        while True:
            step += 1
            env.render()
            action = dqn.act(state)
            state_next, reward, done, info = env.step(action)
            reward = reward if not done else -reward
            state_next = np.reshape(state_next, [1, observation_space])
            dqn.remember(state, action, reward, state_next, done)
            state = state_next
            if done:
                print ("Run: " + str(run) + ", exploration: " + str(dqn.exploration_rate) + ", score: " + str(step))
                score_logger.add_score(step, run)
                break
            dqn.replay_train()


if __name__ == "__main__":
    random.seed(123)
    simulate(seed=234)
